import { ref } from "vue";
import { defineStore } from "pinia";
import type { Product } from "@/types/product";

export const productStore = defineStore("product", () => {
  const sum = ref(0);
  const products = ref(
    Array.from(Array(100).keys()).map((item) => {
      return {
        id: item,
        name: "Product " + (item + 1),
        price: (Math.floor(Math.random() * 100) + 1) * 10,
      };
    })
  );
  function AddProduct(product: Product): void {
    listOrders.value.push(product);
    sum.value += product.price;
  }
  //   function addlistOrders(order: Product): void {
  //     listOrders.value.push(order);
  //     sum.value += order.price;
  //   }
  const listOrders = ref<Product[]>([]);
  return { products, sum, AddProduct, listOrders };
});
